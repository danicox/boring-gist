import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layouts',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Layouts'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(25),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ExampleButton("Centered Column", () {
                  return JustAColumn();
                }),
                fullBreak,
                ExampleButton("Change Center to Align", () {
                  return AlignDoesntWork();
                }),
                fullBreak,
                ExampleButton("Pretty Good with Expanded", () {
                  return FakedWithExpanded();
                }),
                fullBreak,
                ExampleButton("Read the comments, align does work", () {
                  return AlignWorksWithMin();
                }),
                fullBreak,
                ExampleButton("With Intrinsic Width", () {
                  return WithIntrinsicWidth();
                }),
                fullBreak,
                ExampleButton("Cross Alignment Stretch", () {
                  return FinalDesign();
                }),
                fullBreak,
                ExampleButton("Final Design", () {
                  return WithStretch();
                }),
              ],
            ),
          ),
        ),
      ),
    );
}

class ExampleButton extends StatelessWidget {
  const ExampleButton(
    this.exampleName,
    this.exampleClassCreator, {
    Key key,
  }) : super(key: key);

  final String exampleName;
  final Function exampleClassCreator;

  @override
  Widget build(BuildContext context) => RaisedButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => ExamplePages(exampleClassCreator())));
        },
        child: Text(exampleName),
        color: Theme.of(context).primaryColor,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      );
}

class ExamplePages extends StatelessWidget {
  ExamplePages(this.example);

  final Widget example;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: Text("Example"),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(25),
          child: example,
        ),
      ),
    );
}

// I originally Just changed the counter app and added the widgets I needed.
class JustAColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Column(
        // Column is also layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Enter Your Code',
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
          ),
          Text(
            'Tap here to go back and try again',
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(fontWeight: FontWeight.normal, color: Theme.of(context).primaryColor),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
          ),
          RaisedButton(
            child: Text('Next'),
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

// I wanted to push the column up on the screen and a quick search revealed align - which alas did nothing.
// Part of why it did nothing is because it was still centering horizontally which is what Center did.  Besides, the
// TextField took up the entire width, so Center doubly did nothing
class AlignDoesntWork extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, -0.5),
      // Center is a layout widget. It takes a single child and positions it
      // in the middle of the parent.
      child: Column(
        // Column is also layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Enter Your Code',
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
          ),
          Text(
            'Tap here to go back and try again',
            style: Theme.of(context).textTheme.subhead.copyWith(
                  fontWeight: FontWeight.normal,
                  color: Theme.of(context).primaryColor,
                ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
          ),
          RaisedButton(
            child: Text('Next'),
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

// I needed to get things moving, so I figured out how to mostly achieve what I wanted by adding an empty Expanded field on each side
class FakedWithExpanded extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(),
            flex: 1,
          ),
          Text(
            'Enter Your Code',
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
          ),
          Text(
            'Tap here to go back and try again',
            style: Theme.of(context).textTheme.subhead.copyWith(
                  fontWeight: FontWeight.normal,
                  color: Theme.of(context).primaryColor,
                ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: RaisedButton(
              child: Text('Next'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              onPressed: () {},
            ),
          ),
          Expanded(
            child: Container(),
            flex: 2,
          ),
        ],
      ),
    );
  }
}

// When I started my next project, I just knew that Align should work.  At this point I reread the initial comment on
// Column, and saw that *by default* it expanded to fill all available vertical space
// By default means that there is a way to change it, so I read the flutter page on Column and it worked!
class AlignWorksWithMin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, -0.5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Enter Your Code',
            style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
          ),
          Text(
            'Tap here to go back and try again',
            style: Theme.of(context)
                .textTheme
                .subhead
                .copyWith(fontWeight: FontWeight.normal, color: Theme.of(context).primaryColor),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: RaisedButton(
              child: Text('Next'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}

const Widget fullBreak = const SizedBox(height: 25);

// I also was getting complaints especially when landscape mode about the 6 digit field and the button were taking up
// the entire width.  By now, I knew I could put this in a SizedBox and specify a width, but this seemed restrictive and
// inelegant.  Simon on the Flutter study group slack and I worked out this solution, which gets us a column width that
// is what I want as shown by the TextField but the button is wrong
class WithIntrinsicWidth extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, -0.5),
      child: IntrinsicWidth(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Enter Your Code',
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
            ),
            Text(
              'Tap here to go back and try again',
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(fontWeight: FontWeight.normal, color: Theme.of(context).primaryColor),
            ),
            fullBreak,
            TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
            fullBreak,
            RaisedButton(
              child: Text('Next'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}

// The Button can be fixed by putting in a SizedBox with max width, but there has to be a better way ... stretch
class WithStretch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, -0.5),
      child: IntrinsicWidth(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Enter Your Code',
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
            ),
            Text(
              'Tap here to go back and try again',
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(fontWeight: FontWeight.normal, color: Theme.of(context).primaryColor),
            ),
            fullBreak,
            TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
            fullBreak,
            RaisedButton(
              child: Text('Next'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}

// The last one, if you look at it closely, has one Text that is not centered, so this fixes that.
class FinalDesign extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment(0, -0.5),
      child: IntrinsicWidth(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              'Enter Your Code',
              style: Theme.of(context).textTheme.display1.copyWith(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
              textAlign: TextAlign.center,
            ),
            Text(
              'Tap here to go back and try again',
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .copyWith(fontWeight: FontWeight.normal, color: Theme.of(context).primaryColor),
              textAlign: TextAlign.center,
            ),
            fullBreak,
            TextField(
              keyboardType: TextInputType.phone,
              decoration: new InputDecoration.collapsed(
                hintText: 'Enter Code',
                border: UnderlineInputBorder(),
              ),
              style: Theme.of(context).textTheme.display1,
              textAlign: TextAlign.center,
            ),
            fullBreak,
            RaisedButton(
              child: Text('Next'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
